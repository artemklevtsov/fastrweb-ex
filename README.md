## Использование

Запустить сборку и сам контейнер можно с помозью следующей команды:

```bash
docker-compose up
```

Остановить сервис можно с помощью `Ctrl+C`.

Более детальный контроль над контейнером описан ниже.

### Сборка

```bash
docker-compose build
```

### Запуск

```bash
docker-compose start
```

### Остановка

```bash
docker-compose stop
```

## Проверка

### GET

```bash
curl 'http://127.0.0.1:8181/model-get?sex=F&ldose=4'
curl -I 'http://127.0.0.1:8181/model-get?sex=F&ldose=4'
```

### POST 

```bash
 curl -H "Content-Type: application/json" -X POST -d '{"sex": "F", "ldose": 4}' 'http://127.0.0.1:8181/model-post'
 ```
